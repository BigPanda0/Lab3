﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TRPO_Lab3.Lib;
using TRPO_Lab3.WPF.Models;

namespace TRPO_Lab3.WPF.ViewModels
{
    internal class MainWindowVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public MainWindowVM()
        {
            SphereValue = new Sphere();
        }

        private Sphere sphereValue;
        public Sphere SphereValue
        {
            get { return sphereValue; }
            set
            {
                sphereValue = value;
                OnPropertyChanged();
            }
        }

        private MyCommand calculateСommand;
        public MyCommand CalculateСommand
        {
            get
            {
                return calculateСommand ??
                  (calculateСommand = new MyCommand((obg) =>
                  {
                      if (sphereValue.Radius != 0 && sphereValue.RadiusKonusa != 0)
                      {
                          var cal = new Area();



                          SphereValue.Result =Math.Round(Area.PloshadShara(SphereValue.Radius, SphereValue.RadiusKonusa),2);
                      }

                  }));
            }
        }
    }
}
