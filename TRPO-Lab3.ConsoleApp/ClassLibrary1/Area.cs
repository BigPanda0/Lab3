﻿using System;
using System.Linq.Expressions;
using Xunit;

namespace TRPO_Lab3.Lib
{
    public class Area
    {
        public static double PloshadShara(double radius, double radiusKonusa)
        {
            const double Pi = 3.14;
            double result;

            Negative(radius,radiusKonusa);

            result = (2 * Pi * radius * (radius + (radiusKonusa / 2)) - Math.Sqrt(Math.Pow(radius, 2)) + Math.Pow(radius, 2));

            return result;

        }
        public static void Negative(double radius,double radiusKonusa)
        {
            if (radiusKonusa <= 0 || radius<= 0)
            throw new ArgumentException("Нельзя делить на ноль");

        }
    }
}