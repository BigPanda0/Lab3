using NUnit.Framework;
using System;
using TRPO_Lab3.Lib;

namespace TRPO_Lab3.Tests
{
    public class Tests
    {
        [Test]
        public void Test1()
        {
            double radius = 5;
            double radiusKonusa = 3;
            double expected = 224.10000000000002;
            double fallibilityExpected = Math.Round(expected,2);// значение c учета погрешности (224.10)

            double result = Area.PloshadShara(radius, radiusKonusa);
            double fallibilityResult = Math.Round(result, 2); // значение c учета погрешности (224.10)

            Assert.AreEqual(fallibilityExpected, fallibilityResult);
        }

        [Test]
        public void Test2()
        {
            double radius = -1;
            double radiusKonusa = 0;

            Assert.Throws<ArgumentException>(() => Area.PloshadShara(radius, radiusKonusa));

        }
    }
}