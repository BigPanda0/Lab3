﻿using System;
using TRPO_Lab3.Lib;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main(string[] args)
        {

            try
            {
                Console.WriteLine("Введите радиус шара");
                double radiusBall = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Введите радиус основания конуса");
                double radiusConeBase = Convert.ToDouble(Console.ReadLine());
                double result = Area.PloshadShara(radiusBall, radiusConeBase);
                Console.WriteLine(result);

            }
            catch (FormatException)
            {
                Console.WriteLine("Вы ввели буквенные значение. Вам нужно ввести цифры.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
