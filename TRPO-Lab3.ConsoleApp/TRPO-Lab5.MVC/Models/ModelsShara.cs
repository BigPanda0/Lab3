﻿namespace TRPO_Lab5.MVC.Models
{
    public class ModelsShara
    {
        public double RadiusBall { get; set; }
        public double RadiusKonusa { get; set; }
        public double Result { get; set; }

    }
}
