﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using TRPO_Lab3.Lib;
using TRPO_Lab5.MVC.Models;

namespace TRPO_Lab5.MVC.Controllers
{
    public class HomeController : Controller
    {

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(double radius,double radiusKonusa)
        {
            double result;
            result = Area.PloshadShara(radius, radiusKonusa);

            ModelsShara indexViewModel = new ModelsShara
            {
                RadiusBall = radius,
                RadiusKonusa = radiusKonusa,
                Result = result
            };
            return View(indexViewModel);
        }
    }
}